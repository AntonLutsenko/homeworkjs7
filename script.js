// DOM, – это интерфейс, с помощью которого программы могут работать с контентом, структурой и стилями веб-страницы.
const items = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.body;
function outputList(arr, parent) {
	parent.insertAdjacentHTML(
		"beforeend",
		`<ul>${arr.map((item) => `<li>${item}</li>`).join("")}</ul>`
	);
}

outputList(items, parent);
